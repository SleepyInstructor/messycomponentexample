import {Bottle} from "./bottle.class";

//The Jasmine keywords are going to be in red
//If this bugs you, feel free to find out how to fix it
//and write a tutorial to share with the rest of the class.
describe("Placeholder Suite", function () {
    let b = 6;
    const size = 5;
    let bot = new Bottle(size);
    beforeEach(function () {

    });
    it("single bottle fill test", function () {


        bot.fill();
        expect(bot.amt).toBe(size);
    });
    it("emtpy bottle test", function () {
        bot.empty();
        expect(bot.amt).toBe(0);
    });
    it("test pouring", function(){
        let bot1 = new Bottle(5);
        let bot2 = new Bottle(3);
        bot1.fill();
        bot1.pour(bot2);

        expect(bot1.amt).not.toBe(3);
        expect(bot2.amt).toBe(3);
    })


})