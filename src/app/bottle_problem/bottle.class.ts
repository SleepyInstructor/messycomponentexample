export class Bottle {
    amt: number = 0;

    constructor(public size: number) { }
    fill() {
        this.amt = this.size;
    }
    empty() {
        this.amt = 0;
    }

    pour(targetBottle: Bottle) {
        let total = targetBottle.amt + this.amt;

        if (total < targetBottle.size) {
            targetBottle.amt = total;
            this.empty();
        } else {
            targetBottle.fill();
            this.amt = total - targetBottle.amt;
        }
    }
}