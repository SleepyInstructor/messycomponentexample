import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/Forms';

import { AppComponent } from './app.component';
import { BottleappComponent } from './bottleapp/bottleapp.component';


@NgModule({
  declarations: [
    AppComponent,
    BottleappComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
